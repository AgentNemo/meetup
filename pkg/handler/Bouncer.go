package handler

import (
	"crypto/rsa"
	"fmt"
	"gitlab.com/AgentNemo/meetup/pkg/crypto"
	"gitlab.com/AgentNemo/meetup/pkg/logger"
	"net"
	"strings"
	"time"
)

const (
	minMSGLength   = 1     // min msg length
	RSAMSGLength   = 2     // msg length if rsa key is provided
	maxMSGLength   = 2     // max msg length
	connType       = "udp" // connection type
	ringingMSGSize = 2048  // msg ringing size
)

// Represents the decoded message from the user
type SecretMessage struct {
	Length   int    // msg length
	Password string // meetup password
	KeyPub   string // RSA pub key as pem string
}

// Creates a new SecretMessage from the user message.
// The separator defines the substring to split the message.
func NewSecretMessage(msg string, separator string) (*SecretMessage, error) {
	// slit message by separator
	infos := strings.Split(msg, separator)
	// create secret message
	sm := SecretMessage{Length: len(infos)}
	if sm.Length < minMSGLength || sm.Length > maxMSGLength {
		return nil, fmt.Errorf("MSG wrong length")
	}
	sm.Password = infos[0]
	if sm.Length >= RSAMSGLength {
		sm.KeyPub = infos[1]
	}
	return &sm, nil
}

// Represents the bouncer approved user
type Visitor struct {
	Conn   net.Conn       // visitor connection
	KeyPub *rsa.PublicKey // RSA pub key as pem string
}

// Sends the visitor a messages.
// Encrypts the message if a rsa key is provided
func (v *Visitor) Send(msg string) error {
	data := []byte(msg)
	// if rsa key provided encrypt
	if v.KeyPub != nil {
		dataEncrypted, err := crypto.EncryptByPublic(v.KeyPub, data)
		if err != nil {
			return err
		}
		data = dataEncrypted
	}
	_, err := v.Conn.Write(data)
	return err
}

// Reads a user messages. If a rsa key is provided the message is decrypted.
func (v *Visitor) Read() (string, error) {
	buf := make([]byte, 1024)
	// read
	n, err := v.Conn.Read(buf[0:])
	if err != nil {
		return "", err
	}
	// if rsa key provided decrypt
	if v.KeyPub != nil {
		msg := crypto.DecryptByPublic(v.KeyPub, buf[0:n])
		return string(msg), nil
	}
	return string(buf[0:n]), nil
}

// Creates a new Visitor
func NewVisitor(conn net.Conn, pub *rsa.PublicKey) *Visitor {
	return &Visitor{Conn: conn, KeyPub: pub}
}

// Bouncer, checks if user is allowed to enter and converts rsa key
type Bouncer struct {
	id               int    // bouncer id
	messageSeparator string // msg separator
	password         string // password to be approved. Its in SHA256
	counter          uint   // counts the number of msg arrived
	counterApproved  uint   // counts the number of visitors
	connPort         string // bouncer connPort

	// not implemented yet
	pause bool // defines if the bouncer should do a pause
}

// Creates a new Bouncer.
// bouncer id, password as SHA256, connection port to listen to, message separator, radio
func NewBouncer(id int, password string, connPort string, separator string) *Bouncer {
	return &Bouncer{
		id:               id,
		messageSeparator: separator,
		password:         password,
		pause:            false,
		counter:          0,
		counterApproved:  0,
		connPort:         connPort}
}

// Handles the ringing. addr is the address to listen at.
// Through the channel letIn the visitors that are approved get passed.
// If any error occurs during the process the user is not notified.
func (b *Bouncer) Start(letIn chan Visitor) {
	// resolve address to listen to
	addr, err := net.ResolveUDPAddr(connType, b.connPort)
	if err != nil {
		logger.LogE(err)
		//b.sendDead(err)
		return
	}
	// listen to address
	connUDP, err := net.ListenUDP(connType, addr)
	if err != nil {
		logger.LogE(err)
		return
	}
	defer connUDP.Close()

	// hot loop, TODO: change hot loop for graceful shutdown
	for true {
		logger.LogF("Bouncer %d: Waits for ringing at %s", b.id, addr)
		// Reads message
		var buf [ringingMSGSize]byte
		n, addrVisitor, err := connUDP.ReadFrom(buf[0:])
		if err != nil {
			logger.LogE(err)
			continue
		}

		if n == 0 {
			logger.LogF("Bouncer %d: Read zero bytes", b.id)
			continue
		}

		logger.LogF("Bouncer %d: New ringing from %s", b.id, addrVisitor)

		b.counter++

		// handle msg
		msg := string(buf[0:n])

		sm, err := b.checkMessage(msg)
		if err != nil {
			logger.LogE(err)
			continue
		}

		visitor, err := b.convertToVisitor(sm, addrVisitor)
		if err != nil {
			logger.LogE(err)
			continue
		}

		// outsource
		go b.transmitVisitor(visitor, letIn, 5)

	}
	err = fmt.Errorf("bouncer %d: unexcepted error", b.id)
	logger.LogE(err)
}

func (b *Bouncer) transmitVisitor(visitor *Visitor, letIn chan Visitor, attempts int) {
	count := attempts
	for count >= 0 {
		select {
		case letIn <- *visitor:
			b.counterApproved++
			logger.LogF("Bouncer %d approved visitor %d with addr %s", b.id, b.counterApproved, visitor.Conn.RemoteAddr().String())
			return
		default:
			time.Sleep(time.Second)
			count--
		}
	}
	logger.LogF("Bouncer %d: Unable to transmit visitor %s", b.id, visitor.Conn.RemoteAddr().String())
}

// Checks the MSG. If successful returns a new SecretMessage
func (b *Bouncer) checkMessage(msg string) (*SecretMessage, error) {
	// create new SecretMessage
	sm, err := NewSecretMessage(msg, b.messageSeparator)
	if err != nil {
		return nil, err
	}

	// check password
	if !crypto.ComparePass(b.password, sm.Password) {
		return nil, fmt.Errorf("wrong password")
	}

	return sm, nil
}

// Converts the information given by the SecretMessage to a new Visitor
func (b *Bouncer) convertToVisitor(sm *SecretMessage, addr net.Addr) (*Visitor, error) {
	var pubKey *rsa.PublicKey = nil
	var err error = nil
	// if rsa provided parse
	if sm.KeyPub != "" {
		pubKey, err = crypto.ParseRsaPublicKeyFromPemStr(sm.KeyPub)
		if err != nil {
			return nil, err
		}
	}
	addrCallback, err := net.ResolveTCPAddr("tcp", addr.String())
	if err != nil {
		return nil, err
	}
	logger.LogF("Call %s, %s", addr.Network(), addr.String())
	conn, err := net.DialTCP("tcp", nil, addrCallback)
	if err != nil {
		return nil, err
	}
	visitor := NewVisitor(conn, pubKey)
	return visitor, nil
}
