package handler

import (
	"fmt"
	"gitlab.com/AgentNemo/meetup/pkg/crypto"
	"gitlab.com/AgentNemo/meetup/pkg/model"
)

type Waiter struct {
	tables []*model.Table
	users  []*model.User
	roles  []*model.Role
}

func NewWaiter() *Waiter {
	return &Waiter{}
}

// adds new roles
func (w *Waiter) AddRole(role *model.Role) error {
	for _, r := range w.roles {
		if r.Name() == role.Name() {
			return fmt.Errorf("role already exits")
		}
	}
	w.roles = append(w.roles, role)
	return nil
}

// adds new tables
func (w *Waiter) AddTable(table *model.Table) error {
	for _, r := range w.tables {
		if r.Name() == table.Name() {
			return fmt.Errorf("table already exits")
		}
	}
	w.tables = append(w.tables, table)
	return nil
}

func (w *Waiter) NewUser(user *model.User) {
	w.users = append(w.users, user)
	user.CurrentTable().AddUser(user)
}

func (w *Waiter) NewTable(name string, pass string) error {
	table := w.GetTable(name)
	if table != nil {
		return fmt.Errorf("table already exits")
	}
	hash := crypto.Hash(pass)
	w.AddTable(model.NewTable(name, hash))
	return nil
}

func (w *Waiter) IsUsernameFree(username string) bool {
	for _, user := range w.users {
		if user.GetName() == username {
			return false
		}
	}
	return true
}

func (w *Waiter) AssignToTable(user *model.User, table *model.Table, password string) bool {
	if crypto.ComparePass(table.Password(), password) {
		table.AddUser(user)
		return true
	}
	return false
}

func (w *Waiter) RemoveUser(user *model.User) {
	var table *model.Table = nil
	index := 0
	for i, u := range w.users {
		if u.GetName() == user.GetName() {
			index = i
			table = u.CurrentTable()
			break
		}
	}
	w.users[index] = w.users[len(w.users)-1]
	w.users = w.users[:len(w.users)-1]
	table.RemoveUser(user)
}

func (w *Waiter) RemoveTable(table *model.Table) {
	index := 0
	for i, u := range w.tables {
		if u.Name() == table.Name() {
			index = i
			break
		}
	}
	w.tables[index] = w.tables[len(w.tables)-1]
	w.tables = w.tables[:len(w.tables)-1]
}

func (w *Waiter) GetUser(username string) *model.User {
	for _, user := range w.users {
		if user.GetName() == username {
			return user
		}
	}
	return nil
}

func (w *Waiter) GetTable(name string) *model.Table {
	for _, table := range w.tables {
		if table.Name() == name {
			return table
		}
	}
	return nil
}

func (w *Waiter) Users() []*model.User {
	return w.users
}

func (w *Waiter) Tables() []*model.Table {
	return w.tables
}
