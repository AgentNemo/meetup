package handler

import (
	"fmt"
	"gitlab.com/AgentNemo/meetup/pkg/commands"
	"gitlab.com/AgentNemo/meetup/pkg/logger"
	"gitlab.com/AgentNemo/meetup/pkg/model"
	"strings"
	"sync"
)

type Manager struct {
	sync.Mutex
	userCountMax uint // how many users are allowed
	userCount    uint // how many users are in
	waiter       *Waiter
	receptionist []*Receptionist
	parser       *commands.Parser
}

func NewManager(userCountMax uint, waiter *Waiter, parser *commands.Parser, receptionist ...*Receptionist) *Manager {
	return &Manager{userCountMax: userCountMax, userCount: 0, waiter: waiter, receptionist: receptionist,
		parser: parser}
}

func (m *Manager) handleVisit(user model.User) error {
	// add user to the global list
	m.waiter.NewUser(&user)
	// increment counter
	m.incrementUserCount()

	user.Send(fmt.Sprintf("Hello %s, welcome at table %s! You have the role %s",
		user.GetName(), user.CurrentTable().Name(), user.Role().Name()))
	// hot loop for user
	for true {
		msg, err := user.Read()
		if err != nil {
			m.removeUser(&user)
			return err
		}
		msg = strings.TrimSpace(msg)
		if len(msg) == 0 {
			continue
		}
		m.handleCommand(msg, &user)
	}
	return nil
}

func (m *Manager) handleCommand(msg string, user *model.User) {
	value, cmd, err := m.parser.ParseAllowed(msg, user.Role().GetCommands()...)
	if err != nil && cmd != nil {
		user.Send(err.Error())
		return
	}
	switch cmd {
	case commands.MSG:
		user.SendTable(value[0])
	case commands.PMSG:
		pUser := m.waiter.GetUser(value[0])
		if pUser == nil {
			user.Send("User not found")
			return
		}
		pUser.Send(fmt.Sprintf("Private msg from %s: %s", user.GetName(), value[1]))
	case commands.ME:
		user.Send(user.GetInfo())
	case commands.EXIT:
		user.Send("Close connection...")
		m.removeUser(user)
	case commands.GOTO:
		table := m.waiter.GetTable(value[0])
		if table == nil {
			user.Send("table not found")
			return
		}
		successful := m.waiter.AssignToTable(user, table, value[1])
		if !successful {
			user.Send("password incorrect")
			return
		}
	case commands.TABLE:
		user.Send(user.CurrentTable().GetInfo())
	case commands.USER:
		tUser := m.waiter.GetUser(value[0])
		user.Send(tUser.GetInfo())
	case commands.TABLE_CREATE:
		err := m.waiter.NewTable(value[0], value[1])
		if err != nil {
			user.Send("table already exists")
		}
	case commands.TABLE_CLOSE:
		table := m.waiter.GetTable(value[0])
		if table == nil {
			user.Send("table does not exit")
		}
		users := table.GetUsers()
		for _, user := range users {
			user.Send("Close connection...")
			m.waiter.RemoveUser(user)
		}
		m.waiter.RemoveTable(table)
	case commands.USER_KICK:
		tUser := m.waiter.GetUser(value[0])
		if tUser == nil {
			user.Send("user does not exits")
		}
		tUser.Send("Close connection...")
		m.waiter.RemoveUser(tUser)
	case commands.TABLE_PASS:
		table := m.waiter.GetTable(value[0])
		if table == nil {
			user.Send("table does not exit")
		}
		table.ChangePass(value[1])
	default:
		user.SendTable(value[0])
	}
	return
}

func (m *Manager) removeUser(user *model.User) {
	user.Conn().Close()
	m.waiter.RemoveUser(user)
	logger.LogF("User %s disconnected", user.GetName())
	m.decrementUserCount()
}

func (m *Manager) decrementUserCount() error {
	m.Lock()
	defer m.Unlock()
	if m.userCount == 0 {
		return fmt.Errorf("no user should be here")
	}
	m.userCount--
	return nil
}

func (m *Manager) incrementUserCount() error {
	m.Lock()
	defer m.Unlock()
	if m.userCount >= m.userCountMax {
		logger.Log("Server full")
		return fmt.Errorf("full")
	}
	m.userCount++
	return nil
}

func (m *Manager) isFree() bool {
	m.Lock()
	defer m.Unlock()
	return m.userCount < m.userCountMax
}

func (m *Manager) isUsernameFree(name string) bool {
	return m.waiter.IsUsernameFree(name)
}

func (m *Manager) Start() {
	// create channel
	entrance := make(chan model.User, 10)
	for _, receptionist := range m.receptionist {
		// start receptionist and radio
		go receptionist.Start(entrance)
	}

	for true {
		go m.handleVisit(<-entrance)
	}
}

func (m *Manager) Stop() {
	for _, user := range m.waiter.Users() {
		m.removeUser(user)
	}
	for _, table := range m.waiter.Tables() {
		m.waiter.RemoveTable(table)
	}
}
