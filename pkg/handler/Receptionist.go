package handler

import (
	"fmt"
	"gitlab.com/AgentNemo/meetup/pkg/commands"
	"gitlab.com/AgentNemo/meetup/pkg/logger"
	"gitlab.com/AgentNemo/meetup/pkg/model"
	"strings"
)

// Handles the bouncer approved visitors, transform them into users
type Receptionist struct {
	id       int              // receptionist id
	bouncers []*Bouncer       // bouncers for this waiter
	parser   *commands.Parser // command parser
	role     *model.Role      // role for new users
	table    *model.Table     // table for new users
	manager  *Manager
	pause    bool
}

// Creates a new receptionist.
func NewReceptionist(id int, parser *commands.Parser, role *model.Role, table *model.Table, bouncers ...*Bouncer) *Receptionist {
	receptionist := Receptionist{
		id:       id,
		bouncers: make([]*Bouncer, 0),
		parser:   parser,
		manager:  nil,
		role:     role,
		table:    table,
		pause:    false,
	}

	for _, bouncer := range bouncers {
		receptionist.bouncers = append(receptionist.bouncers, bouncer)
	}
	return &receptionist
}

// Starts all bouncers related to this receptionist
func (r *Receptionist) Start(newUser chan model.User) {
	letIn := make(chan Visitor, 10)
	// start all bouncers
	for _, bouncer := range r.bouncers {
		logger.LogF("Start bouncer %d", bouncer.id)
		go bouncer.Start(letIn)
	}
	// hot loop, TODO: change hot loop for graceful shutdown
	for true {
		// call for every new visitor
		go r.handleNewVisitor(newUser, <-letIn)
	}
}

// handles a new visitor
func (r *Receptionist) handleNewVisitor(newUser chan model.User, visitor Visitor) {
	logger.LogF("Receptionist %d: New visitor from %s", r.id, visitor.Conn.RemoteAddr())
	// check if free space
	if !r.isFree() {
		visitor.Send("MeetUp full!")
		visitor.Conn.Close()
		return
	}

	// send welcome message
	welcomeMSG := "Welcome!"
	if visitor.KeyPub != nil {
		welcomeMSG = fmt.Sprintf("%s %s", welcomeMSG, "RSA encryption enabled")
	} else {
		welcomeMSG = fmt.Sprintf("%s %s", welcomeMSG, "RSA encryption disabled")
	}
	err := visitor.Send(welcomeMSG)
	if err != nil {
		logger.LogE(err)
		visitor.Conn.Close()
		return
	}

	// aks for username
	username, err := r.askUsername(visitor)
	if err != nil {
		logger.LogF("Visitor %s disconnected", visitor.Conn.RemoteAddr().String())
		visitor.Conn.Close()
		return
	}

	// convert to user and send
	user := model.NewUser(username, r.role)
	user.SetKey(visitor.KeyPub)
	user.SetConn(visitor.Conn)
	user.SetCurrentTable(r.table)
	logger.LogF("Let user %s in", username)
	newUser <- *user
}

// Checks if the there is space left to let the user in
func (r *Receptionist) isFree() bool {
	return r.manager.isFree()
}

// Checks if the there is space left to let the user in
func (r *Receptionist) isUsernameFree(name string) bool {
	return r.manager.isUsernameFree(name)
}

// Ask the user for his username
func (r *Receptionist) askUsername(visitor Visitor) (string, error) {
	// ask for username
	commandsAllowed := []*model.Command{commands.NAME, commands.HELP}
	for true {
		visitor.Send("Choose your username!")
		msg, err := visitor.Read()
		if err != nil {
			return "", err
		}
		msg = strings.TrimSpace(msg)
		if len(msg) == 0 {
			continue
		}
		// parse command
		value, command, err := r.parser.ParseAllowed(msg, commandsAllowed...)
		if err != nil {
			err := visitor.Send(err.Error())
			if err != nil {
				return "", err
			}
		}

		// check command
		switch command {
		case commands.HELP:
			// send help command
			err := visitor.Send(r.parser.CMDAllowedToString(commandsAllowed...))
			if err != nil {
				return "", err
			}
			continue
		case commands.NAME:
			// check username
			if !r.isUsernameFree(value[0]) {
				err := visitor.Send("Username already in use")
				if err != nil {
					return "", err
				}
				continue
			}
		}

		return value[0], nil
	}
	return "", nil
}

func (r *Receptionist) SetManager(manager *Manager) {
	r.manager = manager
}
