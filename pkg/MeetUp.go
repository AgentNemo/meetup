package pkg

import (
	"gitlab.com/AgentNemo/meetup/pkg/config"
	"gitlab.com/AgentNemo/meetup/pkg/config/parser"
	"gitlab.com/AgentNemo/meetup/pkg/handler"
)

type MeetUp struct {
	manager *handler.Manager
}

func NewMeetUp() (*MeetUp, error) {
	configurator := config.NewConfig(parser.NewFile(""))
	manager, err := configurator.GetConfigDeafult()
	return &MeetUp{manager}, err
}

// starts a new server
func (s *MeetUp) Start() {
	s.manager.Start()
}

func (s *MeetUp) Stop() {
	s.manager.Stop()
}
