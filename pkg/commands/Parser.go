package commands

import (
	"fmt"
	"gitlab.com/AgentNemo/meetup/pkg/model"
	"strings"
)

type Parser struct {
	commandStart   string
	commands       []*model.Command
	defaultCommand *model.Command
}

func NewParser(commandStart string, commands ...*model.Command) *Parser {
	return &Parser{commandStart: strings.TrimSpace(commandStart),
		defaultCommand: MSG, commands: commands}
}

func (p *Parser) Parse(msg string) ([]string, *model.Command, error) {
	// only commandStart string, act like its a normal message
	if len(msg) == len(p.commandStart) {
		return []string{msg}, p.defaultCommand, nil
	}
	// check for commandStart string
	if strings.HasPrefix(msg, p.commandStart) {
		cmdSplit := strings.Split(msg, " ") // split each word to separate command from parameter

		cmdStr := cmdSplit[0][len(p.commandStart):]        // remove commandStart prefix
		cmd, err := p.findCommand(cmdStr, len(cmdSplit)-1) // check if any command can be found with the name and length
		if cmd == nil {
			// no command found
			return []string{msg}, p.defaultCommand, err
		}
		if err != nil {
			// command found but the length doesnt match
			return cmdSplit[1:], cmd, err
		}
		// command found, return text without command
		return cmdSplit[1:], cmd, err

	}
	// doesnt have the commandStart, act like its a normal message
	return []string{msg}, p.defaultCommand, nil
}

// Checks if any command can be found for the given command string and length.
// Returns the command if the name match. If the length doesnt match, an error ist returned on how it should look like
func (p *Parser) findCommand(command string, commandLength int) (*model.Command, error) {
	for _, c := range p.commands {
		if c.CMD == command {
			if c.Length == commandLength {
				return c, nil
			} else {
				return c, fmt.Errorf(fmt.Sprintf("Format: %s %s ", "<command>", c.Format))
			}
		}
	}
	return nil, fmt.Errorf("no command found")
}

// Parses a msg depending on the commands allowed. Returns the strings after the command and the command itself
func (p *Parser) ParseAllowed(msg string, allowed ...*model.Command) ([]string, *model.Command, error) {
	cmdAllowed := false
	value, cmd, err := p.Parse(msg)
	for _, a := range allowed {
		if a == cmd {
			cmdAllowed = true
		}
	}
	if !cmdAllowed {
		err = fmt.Errorf(fmt.Sprintf("Commands allowed:\n%s ", p.CMDAllowedToString(allowed...)))
	}
	return value, cmd, err
}

func (p *Parser) CMDAllowedToString(allowed ...*model.Command) string {
	str := ""
	for _, cmd := range allowed {
		str += fmt.Sprintf("%s%s\n", p.commandStart, cmd.String())
	}
	return strings.TrimSuffix(str, "\n")
}
