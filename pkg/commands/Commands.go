package commands

import "gitlab.com/AgentNemo/meetup/pkg/model"

var (
	// EVERYONE
	HELP = model.NewCommand("help", 0, "", "lists all commands")
	// Only by Visitors
	NAME = model.NewCommand("name", 1, "<name>", "change username")
	// DEFAULT
	MSG  = model.NewCommand("msg", 1, "<msg>", "regular message")
	PMSG = model.NewCommand("pmsg", 2, "<user> <msg>", "private message")
	ME   = model.NewCommand("me", 0, "", "show info about itself")
	EXIT = model.NewCommand("exit", 0, "", "close connection")
	// PATRON
	GOTO  = model.NewCommand("goto", 2, "<table> <password>", "change table")
	TABLE = model.NewCommand("table", 0, "", "show info about current table")
	USER  = model.NewCommand("user", 1, "<user>", "shows user info")
	// STAKEHOLDER
	TABLE_CREATE = model.NewCommand("tableCreate", 2, "<name> <pass>", "create a table")
	TABLE_CLOSE  = model.NewCommand("tableClose", 1, "<name>", "close a table")
	USER_KICK    = model.NewCommand("kick", 1, "<user>", "kicks a user from the server")
	TABLE_PASS   = model.NewCommand("tablePass", 2, "<table> <pass>", "changes the pass of the table")
)

// command, are sorted by permission, DEFAULT -> ADMIN
const (
	// ADMIN
	CMD_SERVER_INFO     = "meetUp"        // shows info about the server
	CMD_SERVER_SHUTDOWN = "meetDown"      // soft shutdown
	CMD_SERVER_KILL     = "meetKill"      // kills the server aks hard shutdown
	CMD_BOUNCERS_INFO   = "bouncers"      // shows info about all bouncers
	CMD_BOUNCER_INFO    = "bouncer"       // shows info about a bouncer
	CMD_BOUNCER_NEW     = "bouncerCreate" // creates a new bouncer
	CMD_BOUNCER_FIRE    = "bouncerFire"   // fires a bouncer
	CMD_BOUNCER_PAUSE   = "bouncerPause"  // switches bouncer pause flag
	CMD_BOUNCER_PASS    = "bouncerPass"   // changes the bouncer pass
	CMD_USER_MAX        = "userMax"       // changes the max user allowed on the server
	CMD_USER_BAN        = "userBan"       // bans a user forever
	CMD_ROLE            = "role"          // changes a user role
	CMD_BROADCAST       = "broadcast"     // send a msg to everyone
	CMD_BROADCAST_ROLE  = "broadcastRole" // send a message to a specified role
	CMD_USER_MOVE       = "move"          // moves a user from table to table

)
