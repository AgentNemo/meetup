package parser

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/AgentNemo/meetup/pkg/logger"
	"os"
	"path/filepath"
)

var (
	configExt         = ".yaml"
	configPathDefault = "./pkg/config/meetUp_config_sample.yaml"
)

type File struct {
	path string
	*viper.Viper
}

func NewFile(path string) *File {
	v := viper.New()
	return &File{path, v}
}

func (f *File) Parse() (map[string]interface{}, error) {
	if len(f.path) == 0 {
		logger.Log("no path given. continue with default path")
		f.path = configPathDefault
	}
	info, err := os.Stat(f.path)
	if os.IsNotExist(err) {
		return nil, err
	}
	if info.IsDir() {
		return nil, fmt.Errorf("%s is a directory", f.path)
	}
	ext := filepath.Ext(f.path)
	if ext != configExt {
		return nil, fmt.Errorf("%s not allowed. only %s", ext, configExt)
	}
	base := filepath.Base(f.path)

	f.SetConfigName(base[:len(base)-len(configExt)])
	f.SetConfigType(configExt[1:])
	f.AddConfigPath(filepath.Dir(f.path))

	err = f.ReadInConfig()
	if err != nil {
		return nil, err
	}
	s := f.AllSettings()
	fmt.Println(s)
	return s, nil
}
