package config

import (
	"gitlab.com/AgentNemo/meetup/pkg/commands"
	"gitlab.com/AgentNemo/meetup/pkg/config/parser"
	"gitlab.com/AgentNemo/meetup/pkg/handler"
	"gitlab.com/AgentNemo/meetup/pkg/model"
)

var (
	ROLE_DEFAULT = model.NewRole("DEFAULT", model.NewPermission(
		commands.MSG,
		commands.PMSG,
		commands.ME,
		commands.EXIT,

		commands.GOTO,
		commands.USER,
		commands.TABLE,

		commands.TABLE_CREATE,
		commands.TABLE_CLOSE,
		commands.USER_KICK,
		commands.TABLE_PASS,
	))

	TABLE_DEFAULT = model.NewTable("DEFAULT", "89dbf71048801678ca4abfbaa3ea8f7c651aae193357a3e23d68e21512cd07f5")
	TABLE_ADMIN   = model.NewTable("ADMIN", "835d6dc88b708bc646d6db82c853ef4182fabbd4a8de59c213f2b5ab3ae7d9be")
)

var (
	envs = []string{"MAX_USER", "BOUNCER_PASSWORD", "BOUNCER_PORT", "BOUNCER_SEPARATOR"}
)

type Config struct {
	parser parser.Parser
}

func NewConfig(parser parser.Parser) *Config {
	return &Config{parser: parser}
}

func (c *Config) GetConfig() (*handler.Manager, error) {
	_, err := c.parser.Parse()
	return nil, err
}

func (c *Config) GetConfigDeafult() (*handler.Manager, error) {
	com := ROLE_DEFAULT.GetCommands()
	com = append(com, commands.NAME)

	parser := commands.NewParser("/", com...)
	waiter := handler.NewWaiter()
	waiter.AddRole(ROLE_DEFAULT)
	waiter.AddTable(TABLE_DEFAULT)
	waiter.AddTable(TABLE_ADMIN)

	bouncer := handler.NewBouncer(1,
		"e7cf3ef4f17c3999a94f2c6f612e8a888e5b1026878e4e19398b23bd38ec221a",
		":10002",
		":")
	receptionist := handler.NewReceptionist(1, parser, ROLE_DEFAULT, TABLE_DEFAULT, bouncer)
	manager := handler.NewManager(uint(50), waiter, parser, receptionist)
	receptionist.SetManager(manager)
	return manager, nil
}
