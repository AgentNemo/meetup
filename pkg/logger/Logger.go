package logger

import (
	"log"
	"os"
)

var logger *log.Logger = nil

func LogInit() {
	logger = log.New(os.Stdout, "", log.LstdFlags)
}

func Log(msg string) {
	if logger != nil {
		logger.Printf("%s \n", msg)
	}
}

func LogE(err error) {
	if logger != nil {
		logger.Printf("%s \n", err)
	}
}

func LogF(format string, v ...interface{}) {
	if logger != nil {
		logger.Printf(format+" \n", v...)
	}
}
