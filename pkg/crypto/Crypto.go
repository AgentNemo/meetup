package crypto

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"math/big"
)

// Hashes a string
func Hash(pass string) string {
	hash := sha256.Sum256([]byte(pass))
	return fmt.Sprintf("%x", hash)
}

// Compare password. Hashes the passPlain and compares with the  passHash
func ComparePass(passHash string, passPlain string) bool {
	if Hash(passPlain) == passHash {
		return true
	}
	return false
}

// Generate RSA Keys
func GenerateRsaKeyPair() (*rsa.PrivateKey, *rsa.PublicKey) {
	privkey, _ := rsa.GenerateKey(rand.Reader, 4096)
	return privkey, &privkey.PublicKey
}

// Export private key to PEM string
func ExportRsaPrivateKeyAsPemStr(privkey *rsa.PrivateKey) string {
	privkey_bytes := x509.MarshalPKCS1PrivateKey(privkey)
	privkey_pem := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: privkey_bytes,
		},
	)
	return string(privkey_pem)
}

// Parse private key from PEM string
func ParseRsaPrivateKeyFromPemStr(privPEM string) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(privPEM))
	if block == nil {
		return nil, errors.New("failed to parse PEM block containing the key")
	}

	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	return priv, nil
}

// Export public key to PEM string
func ExportRsaPublicKeyAsPemStr(pubkey *rsa.PublicKey) (string, error) {
	pubkey_bytes, err := x509.MarshalPKIXPublicKey(pubkey)
	if err != nil {
		return "", err
	}
	pubkey_pem := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PUBLIC KEY",
			Bytes: pubkey_bytes,
		},
	)

	return string(pubkey_pem), nil
}

// Parse public key from PEM string
func ParseRsaPublicKeyFromPemStr(pubPEM string) (*rsa.PublicKey, error) {
	block, _ := pem.Decode([]byte(pubPEM))
	if block == nil {
		return nil, errors.New("failed to parse PEM block containing the key")
	}

	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	switch pub := pub.(type) {
	case *rsa.PublicKey:
		return pub, nil
	default:
		break // fall through
	}
	return nil, errors.New("key type is not RSA")
}

// Encrypt text based on the pub key
func EncryptByPublic(pub *rsa.PublicKey, msg []byte) ([]byte, error) {
	encryptedBytes, err := rsa.EncryptOAEP(
		sha256.New(),
		rand.Reader,
		pub,
		msg,
		nil)
	return encryptedBytes, err
}

// Decrypt text based on the privat key
func DecryptByPrivate(priv *rsa.PrivateKey, msg []byte) ([]byte, error) {
	decryptedBytes, err := priv.Decrypt(nil, msg, &rsa.OAEPOptions{Hash: crypto.SHA256})
	return decryptedBytes, err
}

func EncryptByPrivate(priv *rsa.PrivateKey, msg []byte) ([]byte, error) {
	return rsa.SignPKCS1v15(rand.Reader, priv, crypto.Hash(0), msg)
}

func DecryptByPublic(pub *rsa.PublicKey, msg []byte) []byte {
	c := new(big.Int)
	m := new(big.Int)
	m.SetBytes(msg)
	e := big.NewInt(int64(pub.E))
	c.Exp(m, e, pub.N)
	out := c.Bytes()
	skip := 0
	for i := 2; i < len(out); i++ {
		if i+1 >= len(out) {
			break
		}
		if out[i] == 0xff && out[i+1] == 0 {
			skip = i + 2
			break
		}
	}
	return out[skip:]
}
