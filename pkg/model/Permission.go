package model

type Permission struct {
	commands []*Command
}

func NewPermission(commands ...*Command) *Permission {
	return &Permission{commands: commands}
}

func (p *Permission) GetCommands() []*Command {
	return p.commands
}
