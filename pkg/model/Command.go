package model

import "fmt"

type Command struct {
	CMD         string
	Length      int
	Format      string
	Description string
}

func NewCommand(CMD string, length int, format string, description string) *Command {
	return &Command{CMD: CMD, Length: length, Format: format, Description: description}
}

func (c *Command) String() string {
	return fmt.Sprintf("%s %s", c.CMD, c.Format)
}

func (c *Command) StringLong() string {
	return fmt.Sprintf("%s %s", c.String(), c.Description)
}
