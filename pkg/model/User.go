package model

import (
	"crypto/rsa"
	"fmt"
	"gitlab.com/AgentNemo/meetup/pkg/crypto"
	"net"
	"time"
)

// User entity
type User struct {
	username     string         // public username
	role         *Role          // user role
	conn         net.Conn       // user connection
	currentTable *Table         // current user table
	key          *rsa.PublicKey // rsa key
	uptime       time.Time
}

// Create new user with a role
func NewUser(username string, role *Role) *User {
	return &User{
		username:     username,
		role:         role,
		conn:         nil,
		currentTable: nil,
		key:          nil,
		uptime:       time.Now(),
	}
}

// Sends a string to the user
func (u *User) Send(msg string) error {
	if u.Conn() == nil {
		return fmt.Errorf("no connection")
	}
	if u.key != nil {
		data, err := crypto.EncryptByPublic(u.key, []byte(msg))
		if err == nil {
			_, err := u.Conn().Write(data)
			return err
		}
	}
	_, err := u.Conn().Write([]byte(msg))
	return err
}

func (u *User) Read() (string, error) {
	if u.Conn() == nil {
		return "", fmt.Errorf("no connection")
	}
	buf := make([]byte, 2048)
	i, err := u.Conn().Read(buf[0:])
	if err != nil {
		return "", err
	}
	msg := crypto.DecryptByPublic(u.Key(), buf[:i])
	return string(msg), nil
}

func (u *User) SendTable(msg string) {
	r := u.CurrentTable()
	if r == nil {
		return
	}
	r.SendFromUser(u, msg)
}

func (u *User) GetInfo() string {
	key := "Disabled"
	if u.key != nil {
		key = "Enabled"
	}
	return fmt.Sprintf("Name:%s\nRole:%s\nAddr:%s\nTable:%s\nRSA:%s\nUptime:%f",
		u.username, u.role.Name(), u.conn.RemoteAddr().String(), u.currentTable.Name(), key, time.Now().Sub(u.uptime).Minutes())
}

// Setter and Getter

func (u *User) Key() *rsa.PublicKey {
	return u.key
}

func (u *User) SetKey(key *rsa.PublicKey) {
	u.key = key
}

func (u *User) Role() *Role {
	return u.role
}

func (u *User) SetRole(role *Role) {
	u.role = role
}

func (u *User) GetName() string {
	return u.username
}

func (u *User) Conn() net.Conn {
	return u.conn
}

func (u *User) SetConn(conn net.Conn) {
	u.conn = conn
}

func (u *User) CurrentTable() *Table {
	return u.currentTable
}

func (u *User) SetCurrentTable(currenttable *Table) {
	u.currentTable = currenttable
}
