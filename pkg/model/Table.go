package model

import (
	"fmt"
	"gitlab.com/AgentNemo/meetup/pkg/crypto"
	"strings"
	"sync"
)

const (
	TABLE_DEFAULT          = "DEFAULT"
	TABLE_PASSWORD_DEFAULT = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
)

// Table entity
type Table struct {
	sync.Mutex
	name     string
	users    []*User
	password string
}

// Creates a new Table with a password and users
func NewTable(name string, password string, users ...*User) *Table {
	r := Table{name: name, password: password}
	for _, user := range users {
		r.AddUser(user)
	}
	return &r
}

// Adds a user to the table
func (t *Table) AddUser(user *User) {
	t.users = append(t.users, user)
	user.SetCurrentTable(t)
	t.SendAll(fmt.Sprintf("Table[%s]: Welcome %s!", t.name, user.username))
}

func (t *Table) SendAll(msg string) {
	for _, user := range t.users {
		go user.Send(msg)
	}
}

// Sends a msg to all user in the table
func (t *Table) SendFromUser(user *User, msg string) {
	t.SendAll(fmt.Sprintf("%s: %s", user.username, msg))
}

func (t *Table) Name() string {
	return t.name
}

func (t *Table) Password() string {
	return t.password
}

func (t *Table) GetUsers() []*User {
	return t.users
}

func (t *Table) RemoveUser(user *User) {
	index := 0
	for i, u := range t.users {
		if u == user {
			index = i
			break
		}
	}
	t.users = remove(t.users, index)
	t.SendAll(fmt.Sprintf("Table: Bye %s!", user.username))
}

func (t *Table) ChangePass(pass string) {
	hash := crypto.Hash(pass)
	t.password = hash
	t.SendAll("Password was changed")
}

func (t *Table) GetInfo() string {
	str := fmt.Sprintf("Name:%s\nUsers:%d\n", t.name, len(t.users))
	for _, user := range t.users {
		str += fmt.Sprintf("%s\n", user.GetName())
	}
	return strings.TrimSuffix(str, "\n")
}

func remove(s []*User, i int) []*User {
	s[i] = s[len(s)-1]
	// We do not need to put s[i] at the end, as it will be discarded anyway
	return s[:len(s)-1]
}
