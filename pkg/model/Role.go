package model

type Role struct {
	name       string
	permission []*Permission
}

func NewRole(name string, permission ...*Permission) *Role {
	return &Role{name: name, permission: permission}
}

func (r Role) Name() string {
	return r.name
}

// TODO: refactor, dirty
func (r Role) GetCommands() []*Command {
	cmd := make([]*Command, 0)
	for _, permission := range r.permission {
		cmd = append(cmd, permission.GetCommands()...)
	}
	return cmd
}
