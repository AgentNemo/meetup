package test

import (
	"fmt"
	"gitlab.com/AgentNemo/meetup/client"
	"gitlab.com/AgentNemo/meetup/pkg/crypto"
	"time"
)

func MultiUserConnection() {
	private, pub := crypto.GenerateRsaKeyPair()
	pubStr, _ := crypto.ExportRsaPublicKeyAsPemStr(pub)
	start := time.Now()
	for i := 0; i < 1000; i++ {
		go client.Connect(fmt.Sprintf("Password:%s", pubStr), private, true)
		time.Sleep(time.Millisecond)
	}
	fmt.Println(time.Now().Sub(start))
}

func SingleUserConnection() {
	private, pub := crypto.GenerateRsaKeyPair()
	pubStr, _ := crypto.ExportRsaPublicKeyAsPemStr(pub)

	client.Connect(fmt.Sprintf("Password:%s", pubStr), private, false)
}
