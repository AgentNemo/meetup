# MeetUp

**Note: Abandoned! Feel free to fork and/or contribute.**

Simple chat server over sockets with security in mind.

First project in this category. If you have suggestions feel free to open an issue or a pull request.

## Errors

Config loading/parsing is not finished. I would finish it if I had time.

EDIT: I'm getting motivated again, so I will finish the config parsing in the next days
