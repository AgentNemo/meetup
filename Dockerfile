FROM golang:latest AS builder

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go build -v main.go

FROM alpine:latest

RUN apk add --no-cache \
        libc6-compat
RUN mkdir -p /app
WORKDIR /app
COPY --from=builder /go/src/app/main /app/app