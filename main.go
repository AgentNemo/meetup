package main

import (
	"gitlab.com/AgentNemo/meetup/pkg"
	"gitlab.com/AgentNemo/meetup/pkg/logger"
	"log"
)

const (
	ConnPort = ":8081"
)

func main() {

	// enable logging
	logger.LogInit()

	// create and start server
	s, err := pkg.NewMeetUp()
	if err != nil {
		logger.LogE(err)
		return
	}
	s.Start()

	log.Println("ENDE")

}
