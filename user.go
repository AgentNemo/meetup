package main

import (
	"fmt"
	"gitlab.com/AgentNemo/meetup/client"
	"gitlab.com/AgentNemo/meetup/pkg/crypto"
)

func main() {
	private, pub := crypto.GenerateRsaKeyPair()
	pubStr, _ := crypto.ExportRsaPublicKeyAsPemStr(pub)

	client.Connect(fmt.Sprintf("Password:%s", pubStr), private, false)
}
