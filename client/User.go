package client

import (
	"bufio"
	"crypto/rsa"
	"fmt"
	"gitlab.com/AgentNemo/meetup/pkg/crypto"
	"gitlab.com/AgentNemo/meetup/pkg/logger"
	"math/rand"
	"net"
	"os"
	"strings"
	"time"
	"unsafe"
)

const (
	connType = "tcp"
	connPort = ":10002"
)

// client
func Connect(msg string, key *rsa.PrivateKey, automate bool) {
	//logger.LogInit()
	// ring
	addrUDP, err := net.ResolveUDPAddr("udp", connPort)
	if err != nil {
		logger.LogE(err)
	}
	connUDP, err := net.DialUDP("udp", nil, addrUDP)
	addrServer := connUDP.LocalAddr()
	if err != nil {
		logger.LogE(err)
	}
	connUDP.Write([]byte(msg))
	connUDP.Close()

	// connection
	addrTCP, err := net.ResolveTCPAddr(connType, addrServer.String())
	if err != nil {
		logger.LogE(err)
	}
	logger.LogF("LISTEN %s, %s", addrTCP.Network(), addrTCP.String())
	listener, err := net.ListenTCP(connType, addrTCP)
	defer listener.Close()
	if err != nil {
		logger.LogE(err)
		os.Exit(1)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}
		handleClient(conn, key, automate)
		conn.Close() // we're finished
	}
}

func handleClient(conn net.Conn, key *rsa.PrivateKey, automate bool) {
	if !automate {
		go handleInput(conn, key)
	} else {
		go handleAutomate(conn, key)
	}
	buf := make([]byte, 2048)
	for true {
		i, err := conn.Read(buf[0:])
		if err != nil {
			continue
		}
		msg, _ := crypto.DecryptByPrivate(key, buf[:i])
		fmt.Printf("%s\n", string(msg))
	}
}

func handleAutomate(conn net.Conn, key *rsa.PrivateKey) {
	time.Sleep(time.Second)
	name := RandStringBytesMaskImprSrcUnsafe(10)
	write(conn, key, fmt.Sprintf("/name %s", name))
	time.Sleep(time.Second)
	for true {
		write(conn, key, name)
		time.Sleep(time.Millisecond)
	}
}

func handleInput(conn net.Conn, key *rsa.PrivateKey) {
	for true {
		reader := bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')
		text = strings.TrimSpace(text)
		write(conn, key, text)
	}
}

func write(conn net.Conn, key *rsa.PrivateKey, text string) {
	msg, _ := crypto.EncryptByPrivate(key, []byte(text))
	conn.Write(msg)
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

func RandStringBytesMaskImprSrcUnsafe(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}
