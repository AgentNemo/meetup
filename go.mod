module gitlab.com/AgentNemo/meetup

go 1.15

require (
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20200918174421-af09f7315aff // indirect
)
